<?php
$curentPage = $_SERVER['REQUEST_URI'];
$home = "#top";
$about = "#about";
$services = "#portfolio";
$contact = "#contact";
if(strstr($curentPage, 'shop')==0){
    $home = "index.php".$home;
    $about = "index.php".$about;
    $services = "index.php".$services;
    $contact = "index.php".$contact;
}
echo ' <!-- Navigation 189437-->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="'.$home.'">Jule\'s</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="'.$about.'">About</a>
                    </li>
                    <li>
                        <a href="'.$services.'">Services</a>
                    </li>
                    <li>
                        <a href="'.$contact.'">Contact</a>
                    </li>
                    <li>
                        <a href="shop.php">Make an order</a>
                    </li>
                    <li>
                        <a href="cart.php">Cart</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>'
?>