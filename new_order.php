<?php
session_start();
include ('BDD.php');
$card    = $_SESSION['cart'];
$name    = $_POST['name'];
$email   = $_POST['email'];
$phone   = $_POST['phone'];
$hour    = $_POST['hour'];
$message = $_POST['message'];
$date    = $_POST['date'];

//Enregistrement de la commande
$req = $BDD->prepare("INSERT INTO commande (nom, mail, tel, com, heure, date, sell) VALUES (:nom, :mail, :tel, :com, :heure, :date ,'false')");
$req->execute(array(
    "nom" => $name,
    "mail" => $email,
    "tel" => $phone,
    "com" => $message,
    "heure" => $hour,
    "date" => $date
));

setcookie('name',$name,time()+3600*24*60);
setcookie('email',$email,time()+3600*24*60);
setcookie('phone',$phone,time()+3600*24*60);

//Recuperation de l'id de la commande
$id = $BDD->lastInsertId('id');

//Enregistrement des produits de la commande
$str = "INSERT INTO liste_commande (id_produit, id_commande, quantity) VALUES ";
foreach ($card as $c){
    $str.="(".$c[0].",".$id.",".$c[1]."),";
}
$str = rtrim($str, ",");
$req = $BDD->prepare($str);
$req->execute();

$_SESSION['cart'] = [];
//TODO rediriger vers page validation
?>