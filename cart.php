<?php
session_start();
include ('BDD.php');

if(!empty($_SESSION['cart'])){
    $cart = $_SESSION['cart'];
    $req = "id=".$cart[0][0];
    for($i=1;$i<count($cart);$i++){
        $req.=" OR id=".$cart[$i][0];
    }
    $reponse = $BDD->query('SELECT * FROM products WHERE '.$req);
    $liste_produits = array();
    while ($res = $reponse->fetch(PDO::FETCH_ASSOC)) {
        $liste_produits[] = $res;
    }
}
$name  = "";
$email = "";
$phone = "";
if (!empty($_COOKIE['name'])){
    $name  = $_COOKIE['name'];
}
if (!empty($_COOKIE['email'])){
    $email = $_COOKIE['email'];
}
if (!empty($_COOKIE['phone'])){
    $phone = $_COOKIE['phone'];
}
date_default_timezone_set('America/Guadeloupe');
$today = date('m/d/Y');
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Shopping Cart</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="css/cart-bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/cart-custom.css"/>
    <link href="css/shop-bootstrap.min.css" rel="stylesheet">
    <link href="css/order.css" rel="stylesheet">
    <link href="css/datepicker.css" rel="stylesheet">

</head>

<body>

<?php include('menu_bar.php'); ?>

</br></br></br>
</br></br>
</br></br>

<div class="container text-center">

<!--    <div class="col-md-5 col-sm-12">-->
<!--        <div class="bigcart"></div>-->
<!--        <h1>Your cart</h1>-->
<!--    </div>-->
    <?php
    $divA = "order";
    $divB = "order";
    if(empty($cart)){
        $divA = "";
    }
    else{
        $divB = "";
    }
    ?>
    <div class="<?php echo $divA; ?>">
        <img src="img/empty_cart.png" class="img-responsive" style="display: block; margin-left: auto; margin-right: auto;">
        </br>
        <a href="shop.php" class="btn btn-dark-inverse">Make an order</a>
    </div>

    <div class="col-md-7 col-sm-12 text-left <?php echo $divB; ?>">
        <ul>
            <li class="row list-inline columnCaptions">
                <span>QTY</span>
                <span>ITEM</span>
                <span>Price</span>
            </li>
            <?php
                $total = 0;
                for($i=0;$i<count($cart);$i++){
                    echo'
                    <li class="row">
                        <span class="quantity">'.$cart[$i][1].'</span>
                        <span class="itemName">'.$liste_produits[$i]["label"].'</span>
                        <span class="popbtn"><a class="arrow"></a></span>
                        <span class="price">$'.number_format ($liste_produits[$i]["price"],2).'</span>
                    </li>';
                    $total+=$cart[$i][1]*$liste_produits[$i]["price"];
                }

                echo'
                 <li class="row totals">
                     <span class="itemName">Total:</span>
                     <span class="price">$'.$total.'</span>
                     <span class="orderButton" onclick="showOrder();"><a class="text-center">ORDER</a></span>
                 </li>';

            ?>
        </ul>
    </div>

</div>

<div id="order" class="order">
    <h1>Order</h1>
    <form class="cf">
        <div class="half left cf">
            <input type="text" id="input-name" placeholder="Name" value="<?php echo $name; ?>">
            <input type="email" id="input-email" placeholder="Email address" value="<?php echo $email; ?>">
            <input type="tel" id="input-phone" placeholder="Phone number" value="<?php echo $phone; ?>" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$">
            <div class="input-append date" id="dp2" data-date="<?php echo $today; ?>" data-date-format="dd-mm-yyyy">
                <input class="span2" size="16" type="text" value="<?php echo $today; ?>" readonly="" x-webkit-speech="" id="datepicker">
            </div>
        </div>
        <div class="half right cf">
            <select id="input-hour">
                <option>Select an hour</option>
            <?php
                $h_debut = 10;
                $h_fin = 18;
                for ($h=$h_debut; $h<$h_fin;$h++){
                    for ($m=0; $m<60;$m+=10) {
                        echo '<option>'.$h . ":" . sprintf('%02d',$m).'</option>';
                    }
                }
            ?>
            </select>
            <textarea name="message" type="text" id="input-message" placeholder="Message (optional)" maxlength="120"></textarea>
        </div>
        <input type="submit" value="Validate" id="input-submit">
    </form>
    <h2 id='result'></h2>
</div>

<!-- The popover content -->

<div id="popover" style="display: none">
    <a href="#"><span class="glyphicon glyphicon-pencil"></span></a>
    <a href="#"><span class="glyphicon glyphicon-remove"></span></a>
</div>

<!-- JavaScript includes -->

<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="js/cart-bootstrap.min.js"></script>
<script src="js/cart-customjs.js"></script>
<script src="js/order.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script>
    $('#datepicker').datepicker();
</script>

</body>
</html>
