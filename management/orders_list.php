<?php
session_start();
if(!empty($_SESSION['admin'])){
    if (!$_SESSION['admin']) {
        header('Location: login.php');
    }
}
else{
    header('Location: login.php');
}

include ('../BDD.php');
if (!empty($_POST["page"])) {
    $page_selected = $_POST["page"];
}
else {
    $page_selected = "curent";
}
if (!empty($_POST["categorie"]))
    $categorie_selected = $_POST["categorie"];
else
    $categorie_selected = 1;//TODO modifier

if($page_selected == "curent" || $page_selected == "sold"){

    $operator = "=";
    if($page_selected == "sold"){
        $operator = "<>";//Attention dans certaines versions de SQL il s'ecrit !=
    }
    //Recuperation des categories
    $reponse = $BDD->query('SELECT * FROM commande WHERE sell'.$operator.'"false" ORDER BY heure');
    $liste_commandes = array();

    while ($res = $reponse->fetch(PDO::FETCH_ASSOC)) {
        //Recuperation des produits de la commande
        $rep = $BDD->query('SELECT quantity, label FROM liste_commande, products WHERE id_commande = '.$res["id"]. ' AND id_produit=id');
        $liste_produits = array();
        while ($res2 = $rep->fetch(PDO::FETCH_ASSOC)) {
            $liste_produits[] = $res2;
        }
        $liste_commandes[] = [$res,$liste_produits];
    }
}
else if($page_selected == "manage"){

    //Ajout d'une categorie
    if (!empty($_POST["categorieAdd"])) {
        $req = $BDD->prepare("INSERT INTO categorie (label) VALUES (:label)");
        $req->execute(array(
            "label" => $_POST["categorieAdd"]
        ));
    }
    elseif (!empty($_POST["productNameAdd"]) && !empty($_POST["productPriceAdd"])){
        $pictue = uploadImage($_FILES['fichier']);
        $req = $BDD->prepare("INSERT INTO products (id_categorie, label, description, price, picture) VALUES (:id_categorie, :label, :description, :price, :picture)");
        $req->execute(array(
            "id_categorie" => $categorie_selected,
            "label" => $_POST["productNameAdd"],
            "description" => $_POST["productDescriptionAdd"],
            "price" => $_POST["productPriceAdd"],
            "picture" => $pictue
        ));
    }
    $reponse = $BDD->query('SELECT * FROM categorie');
    $liste_categorie = array();
    while ($res = $reponse->fetch(PDO::FETCH_ASSOC)) {
        $liste_categorie[] = $res;
    }

//Recuperation des produits de la categorie
    $reponse = $BDD->query('SELECT * FROM products WHERE id_categorie = '.$categorie_selected);
    $liste_produits = array();
    while ($res = $reponse->fetch(PDO::FETCH_ASSOC)) {
        $liste_produits[] = $res;
    }
}

?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Orders</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/shop-bootstrap.min.css" rel="stylesheet">
    <!--    <link href="css/menu-bar.css" rel="stylesheet">-->

    <!-- Custom CSS -->
    <link href="../css/admin-order-homepage.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Navigation -->
<?php include('nav_bar.php')?>


<!-- Page Content -->
<div class="container">

    <div class="row">

        <?php
        if($page_selected == "curent") {
            menu_curent();
        }
        else if($page_selected == "manage"){
            menu_manage($liste_categorie, $categorie_selected);
        }
        ?>
        <div class="col-md-9">

            <div class="row">

                <?php


                if($page_selected == "curent" || $page_selected == "sold") {
                    items_curent_sold($liste_commandes, $page_selected);
                }
                else if($page_selected == "manage"){
                    items_manage($liste_produits, $categorie_selected);
                }
                ?>

            </div>

        </div>

    </div>

</div>
<!-- /.container -->

<div class="container">

    <hr>

    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright &copy; Your Website 2014</p>
            </div>
        </div>
    </footer>

</div>

<div id="snackbar">Item(s) add!</div>

<!-- /.container -->

<!-- jQuery -->
<script src="../js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../js/bootstrap.min.js"></script>
<script src="../js/manage-orders.js"></script>

</body>

</html>

<?php
function menu_curent()
{
    echo '
            <div class="col-md-3">
                <p class="lead">Days</p>
                <div class="list-group">';

    //foreach ($liste_commandes as $value){
    //$selected = "list-group-item";
    //if($value['id']==$categorie_selected){
    $selected = "categorie_selected";
    //}
    echo '
                            <form id="categorie'/*.$value["id"]*/ . '" action="shop.php" method="post">
                                <input type="hidden" name="categorie" value="'/*.$value["id"]*/ . 'Today"/>
                            </form>';
    echo '<a href="#" onclick="document.getElementById(\'categorie'/*.$value["id"]*/ . '\').submit()" class="' . $selected . '">'/*.$value["label"]*/ . 'Today</a>                    
                </div>
            </div>';
}

/**
 * @param $liste_categorie
 * @param $categorie_selected
 */
function menu_manage($liste_categorie, $categorie_selected)
{
    echo '<div class="col-md-3">
            <p class="lead">Categories</p>
                <div class="list-group" id="list-group">';
    foreach ($liste_categorie as $value) {
        if ($value['id'] == $categorie_selected) {
            echo '<div class="categorie_selected" id="category' . $value["id"] . '">';
            echo '    <input type="text" id="inputCategory" name="newName" value="' . $value["label"] . '" placeholder="Category name" style="background-color: #bce8f1;border: 1px;color:black; width:85%">';
            echo '    <input type="hidden" id="selectedCategory" value="'.$value["id"].'"/>';
            echo '<img src="../img/rm.png" style="height: 30px;" onclick="confirmation(\'' . $value["id"] . '\', \'categorie\')">';
            echo '</div>';
        }
        else{
            echo '<div class="list-group-item" id="category' . $value["id"] . '">
                <div onclick="document.getElementById(\'categorie' . $value["id"] . '\').submit()">
                        <form id="categorie' . $value["id"] . '" action="orders_list.php" method="post">
                            <input type="hidden" name="categorie" value="' . $value["id"] . '"/>
                             <input type="hidden" name="page" value="manage"/>
                        </form>';
            echo '      <a href="#" >' . $value["label"] . '</a>
                </div>
            </div>';
        }
    }
    /*echo '  <div class="list-group-item" id="divNewCate">
                <input type="text" name="newCate" id="newCate" placeholder="New categorie">
                <button onclick="addCategorie()">Add</button>
            </div>'; */
    echo '  <div class="list-group-item" id="divNewCate">
                <form action="orders_list.php" method="post">
                    <input type="text" name="categorieAdd" id="categorieAdd" placeholder="New categorie">
                    <input type="hidden" name="page" value="manage"/>
                    <input type="submit" name="submit" value="Add" />
                </form>
            </div>';

    echo '</div>
                </div>';
}

/**
 * @param $liste_commandes
 */
function items_curent_sold($liste_commandes, $page_selected)
{
    foreach ($liste_commandes as $commande) {
        echo '<div class="col-sm-4 col-lg-4 col-md-4" id="order' . $commande[0]['id'] . '">
                <div class="thumbnail">
                    <div class="caption">
                        <h4 class="pull-right">' . $commande[0]['heure'] . '</h4>
                        <h4>[' . $commande[0]['id'] . '] ' . $commande[0]['nom'] . '</h4>';
        foreach ($commande[1] as $produit) {
            echo '<p><b>' . $produit['quantity'] . '</b> - ' . $produit['label'] . '</p>';
        }
        echo '<HR size=3 align=center width="100%">';
        echo '<p>' . $commande[0]['com'] . '</p>';

        echo '<HR size=3 align=center width="100%">';
        echo '<p style="text-align: center">' . $commande[0]['tel'] . '</p>';

        //TODO
        if($page_selected == "sold"){
            echo '<button class="sell" onclick="sell(\'' . $commande[0]["id"] . '\', \'unsell\');">
                      Not sell
                  </button>';
        }
        else{
            echo '<button class="sell" onclick="sell(\'' . $commande[0]["id"] . '\', \'sell\');">
                      Sell
                  </button>';
        }

        echo'
                    </div>
                </div>
            </div>';
    }
}

/**
 * @param $liste_produits
 */
function items_manage($liste_produits, $categorie_selected)
{
    foreach ($liste_produits as $produit) {
        //TODO modifier photo
        echo '<div class="col-sm-4 col-lg-4 col-md-4" id="procuct'.$produit["id"].'">
                    <div class="thumbnail">
                        <img src="../img/'.$produit["picture"].'" alt="" style="height:165px">
                            <div class="caption">
                                <h4 class="pull-right">$' . number_format($produit["price"], 2) . '</h4>
                                <h4>' . $produit["label"] . '</h4>
                                <p class="description">' . $produit["description"] . '</p>
                                <!--<p class="pull-right"><button>Add to card</button></p>-->
                                <p style=\'width=100%;\'>
                                    <form id="product'.$produit["id"].'" action="edit_products.php" method="post">
                                        <input type="hidden" name="id" value="'.$produit["id"].'"/>
                                    </form>
                                    <a class="btn btn-primary button button_add" onclick="document.getElementById(\'product'.$produit["id"].'\').submit()">
                                        Edit
                                    </a>
                                </p>
                                <p class="pull-right">
                                    <a class="btn btn-primary button delete" onclick="confirmation(' . $produit["id"] . ', \'product\')">
                                        Remove
                                    </a>
                                </p>
                            <div>
                        </div>
                    </div>
                </div>
            </div>';
    }
    echo '<div class="col-sm-4 col-lg-4 col-md-4">
                    <div class="thumbnail">
                        <form action="orders_list.php" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="page" value="manage"/>
                            <input type="hidden" name="categorie" value="'.$categorie_selected.'"/>
                            <p>
                                <label for="fichier_a_uploader" title="Choose a picture"></label>
                                <input name="fichier" type="file" id="fichier_a_uploader" placeholder="Product name"/>
                            </p>
                            <div class="caption">
                                <h4><input type="text" name="productNameAdd" id="productNameAdd" placeholder="Product name"></h4>
                                <h4><input type="number" step="0.01" name="productPriceAdd" id="productPriceAdd" placeholder="Price"></h4>
                                <p class="description">
                                    <textarea name="productDescriptionAdd" rows="5" cols="28" placeholder="Description" maxlength="80"></textarea>
                                </p>
                                </br>
                                </br>
                                <p class="pull-right">
                                    <input type="submit" name="submit" value="Add" />
                                </p>
                            <div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>';

}


function uploadImage($img){
    var_dump($img);
    $tabExt = array('jpg','gif','png','jpeg');    // Extensions autorisees
    define('MAX_SIZE', 100000);    // Taille max en octets du fichier
    define('WIDTH_MAX', 800);    // Largeur max de l'image en pixels
    define('HEIGHT_MAX', 800);    // Hauteur max de l'image en pixels

    // On verifie si le champ est rempli
    if( !empty($img['name'])) {
        // Recuperation de l'extension du fichier
        $extension  = pathinfo($img['name'], PATHINFO_EXTENSION);

        // On verifie l'extension du fichier
        if(in_array(strtolower($extension),$tabExt)) {
            // On recupere les dimensions du fichier
            $infosImg = getimagesize($img['tmp_name']);

            // On verifie le type de l'image
            if($infosImg[2] >= 1 && $infosImg[2] <= 14) {
                // On verifie les dimensions et taille de l'image
                if(($infosImg[0] <= WIDTH_MAX) && ($infosImg[1] <= HEIGHT_MAX) && (filesize($img['tmp_name']) <= MAX_SIZE)){
                    // Parcours du tableau d'erreurs
                    if(isset($img['error'])
                        && UPLOAD_ERR_OK === $img['error'])
                        {
                        // On renomme le fichier
                        $nomImage = md5(uniqid()) .'.'. $extension;
                            // Si c'est OK, on teste l'upload
                            if(move_uploaded_file($img['tmp_name'], "../img/".$nomImage))
                            {
                                echo $message = 'Upload réussi !';
                                return $nomImage;
                                //$message = 'Upload réussi !';
                            }
                            else
                            {
                                // Sinon on affiche une erreur systeme
                                $message = 'Problème lors de l\'upload !';
                            }
                        }
                        else
                        {
                            $message = 'Une erreur interne a empêché l\'uplaod de l\'image';
                        }
                    }
                    else
                    {
                        // Sinon erreur sur les dimensions et taille de l'image
                        $message = 'Erreur dans les dimensions de l\'image !';
                    }
                }
                else
                {
                    // Sinon erreur sur le type de l'image
                    $message = 'Le fichier à uploader n\'est pas une image !';
                }
            }
            else
            {
                // Sinon on affiche une erreur pour l'extension
                $message = 'L\'extension du fichier est incorrecte !';
            }
        }
        else
        {
            // Sinon on affiche une erreur pour le champ vide
            $message = 'Veuillez remplir le formulaire svp !';
            //TODO image par defaut
        }
        echo $message;
        return "defaut.png";//TODO preciser extention image
}
?>