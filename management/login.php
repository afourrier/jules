<?php
session_start();
if(!empty($_SESSION['admin'])){
    if ($_SESSION['admin']) {
        header('Location: orders_list.php');
    }
}
if(!empty($_POST['code'])){
    $code = $_POST['code'];

    date_default_timezone_set('America/Guadeloupe');

    $attendue1 = date('dm');
    $attendue2 = date('md');
    if(strcmp($code,$attendue1)==0 || strcmp($code,$attendue2)==0){
        $_SESSION['admin'] = true;
        header('Location: orders_list.php');
    }
}
/**
 * Created by PhpStorm.
 * User: Alexis
 * Date: 21/12/2016
 * Time: 11:18
 */
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/login.css" rel="stylesheet">

</head>

<body>
    </br>
    </br>
    </br>
    <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="loginmodal-container">
                <h1>Login</h1><br>
                <form method="post" target="login.php">
                    <input type="password" name="code" placeholder="Code">
                    <input type="submit" name="login" class="login loginmodal-submit" value="Login">
                </form>
            </div>
        </div>
    </div>
</body>
