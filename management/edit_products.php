<?php
session_start();
if(!empty($_SESSION['admin'])){
    if (!$_SESSION['admin']) {
        header('Location: login.php');
    }
}
else{
    header('Location: login.php');
}

include ('../BDD.php');

if (!empty($_POST["id"])) {
    $id = $_POST["id"];
    $reponse = $BDD->query('SELECT * FROM products WHERE id = '.$id);
    $produits = array();
    while ($res = $reponse->fetch(PDO::FETCH_ASSOC)) {
        $produits[] = $res;
    }
    $reponse = $BDD->query('SELECT * FROM categorie');
    $liste_categorie = array();
    while ($res = $reponse->fetch(PDO::FETCH_ASSOC)) {
        $liste_categorie[] = $res;
    }
}


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Edit</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/shop-bootstrap.min.css" rel="stylesheet">
    <!--    <link href="css/menu-bar.css" rel="stylesheet">-->

    <!-- Custom CSS -->
    <link href="../css/edit-products.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<h1><?php echo $produits[0]["label"]; ?></h1>
<form class="cf" action="manage_products.php" method="post" enctype="multipart/form-data">
    <img src="../img/<?php echo $produits[0]["picture"]; ?>" class="img-responsive">
    <p>
        <label for="fichier_a_uploader" title="Choose a picture"></label>
        <input name="fichier" type="file" id="fichier_a_uploader" placeholder="Product name"/>
    </p>
    <div class="half left cf">
        <input type="text" id="name" name="name" placeholder="Name" value="<?php echo $produits[0]["label"]; ?>">
        <input type="number" name="price" step="0.01" id="price" placeholder="Price" value="<?php echo $produits[0]["price"]; ?>">
        <select id="categorieSelected"  name="categorieSelected">
            <?php
            foreach ($liste_categorie as $cate){
                $selected = "";
                if($produits[0]["id_categorie"]==$cate["id"]){
                    $selected = "selected";
                }
                echo '<option value="'.$cate['id'].'" '.$selected.'>'.$cate['label'].'</option>';
            }
            ?>
        </select>
    </div>
    <div class="half right cf">
        <textarea name="desc" type="text" id="desc" placeholder="Description" value="<?php echo $produits[0]["description"]; ?>"><?php echo $produits[0]["description"]; ?></textarea>
    </div>
    <input type="hidden" name="id" value="<?php echo $id; ?>"/>
    <input type="hidden" name="action" value="alter"/>
    <input type="hidden" name="type" value="product"/>
    <input type="hidden" name="curentImg" value="<?php echo $produits[0]["picture"]; ?>"/>
    <input type="submit" value="Validate" class="input-submit">
</form>
<p class="input-submit" >
    <form id="back" action="orders_list.php" method="post">
        <input type="hidden" name="page" value="curent"/>
        <input type="hidden" name="categorie" value="<?php echo $produits[0]["id_categorie"]; ?>"/>
    </form>
    <a class="input-submit" onclick="document.getElementById('back').submit()" style="margin-left: 48%">
        Cancel
    </a>
</p>
</body>
