<?php
session_start();
if(!empty($_SESSION['admin'])){
    if (!$_SESSION['admin']) {
        header('Location: login.php');
    }
}
else{
    header('Location: login.php');
}

include ('../BDD.php');
$action = $_POST['action'];
$type = $_POST['type'];

if ($action == 'remove' && $type == 'product'){
    $reponse = $BDD->query('SELECT picture FROM products WHERE id_categorie=' . $id);
    $liste_pictures = array();
    while ($res = $reponse->fetch(PDO::FETCH_ASSOC)) {
        $liste_pictures[] = $res;
    }

    $id = $_POST['id'];
    $req = $BDD->prepare("DELETE FROM products WHERE id=" . $id);
    $req->execute();

    //On supprime les photos
    removePictures($liste_pictures);

}
else if ($action == 'remove' && $type == 'categorie'){
    $id = $_POST['id'];
    $req = $BDD->prepare("DELETE FROM categorie WHERE id=" . $id);
    $req->execute();

    $reponse = $BDD->query('SELECT picture FROM products WHERE id_categorie=' . $id);
    $liste_pictures = array();
    while ($res = $reponse->fetch(PDO::FETCH_ASSOC)) {
        $liste_pictures[] = $res;
    }

    $req = $BDD->prepare("DELETE FROM products WHERE id_categorie=" . $id);
    $req->execute();

    //On supprime les photos
    removePictures($liste_pictures);


}
else if ($action == 'alter' && $type == 'product'){
    $id = $_POST['id'];
    $name = $_POST['name'];
    $price = $_POST['price'];
    $desc = $_POST['desc'];
    $id_cate = $_POST['categorieSelected'];
    $curentImg = $_POST['curentImg'];

    $img = uploadImage($_FILES['fichier']);
    $requette = "UPDATE products SET id_categorie='".$id_cate."',label='".$name."',description='".$desc."',price='".$price."'";
    if($img!=""){//Nouvelle image
        if($curentImg!="defaut.png"){
            //On supprime l'image
            unlink("../img/".$curentImg);
        }
        $requette.=",picture='$img'";
    }
    echo $requette." WHERE id=" . $id;
    $req = $BDD->prepare($requette." WHERE id=" . $id);
    $req->execute();

    header('Location: orders_list.php');
}
else if ($action == 'alter' && $type == 'categorie'){
    $id = $_POST['id'];
    $value = $_POST['value'];

    $req = $BDD->prepare("UPDATE categorie SET label='".$value."' WHERE id=" . $id);
    $req->execute();
}

function uploadImage($img){
    var_dump($img);
    $tabExt = array('jpg','gif','png','jpeg');    // Extensions autorisees
    define('MAX_SIZE', 100000);    // Taille max en octets du fichier
    define('WIDTH_MAX', 800);    // Largeur max de l'image en pixels
    define('HEIGHT_MAX', 800);    // Hauteur max de l'image en pixels

    // On verifie si le champ est rempli
    if( !empty($img['name'])) {
        // Recuperation de l'extension du fichier
        $extension  = pathinfo($img['name'], PATHINFO_EXTENSION);

        // On verifie l'extension du fichier
        if(in_array(strtolower($extension),$tabExt)) {
            // On recupere les dimensions du fichier
            $infosImg = getimagesize($img['tmp_name']);

            // On verifie le type de l'image
            if($infosImg[2] >= 1 && $infosImg[2] <= 14) {
                // On verifie les dimensions et taille de l'image
                if(($infosImg[0] <= WIDTH_MAX) && ($infosImg[1] <= HEIGHT_MAX) && (filesize($img['tmp_name']) <= MAX_SIZE)){
                    // Parcours du tableau d'erreurs
                    if(isset($img['error'])
                        && UPLOAD_ERR_OK === $img['error'])
                    {
                        // On renomme le fichier
                        $nomImage = md5(uniqid()) .'.'. $extension;
                        // Si c'est OK, on teste l'upload
                        if(move_uploaded_file($img['tmp_name'], "../img/".$nomImage))
                        {
                            echo $message = 'Upload réussi !';
                            return $nomImage;
                            //$message = 'Upload réussi !';
                        }
                        else
                        {
                            // Sinon on affiche une erreur systeme
                            $message = 'Problème lors de l\'upload !';
                        }
                    }
                    else
                    {
                        $message = 'Une erreur interne a empêché l\'uplaod de l\'image';
                    }
                }
                else
                {
                    // Sinon erreur sur les dimensions et taille de l'image
                    $message = 'Erreur dans les dimensions de l\'image !';
                }
            }
            else
            {
                // Sinon erreur sur le type de l'image
                $message = 'Le fichier à uploader n\'est pas une image !';
            }
        }
        else
        {
            // Sinon on affiche une erreur pour l'extension
            $message = 'L\'extension du fichier est incorrecte !';
        }
    }
    else
    {
        // Sinon on affiche une erreur pour le champ vide
        $message = 'Veuillez remplir le formulaire svp !';
        //TODO image par defaut
        return "";
    }
    echo $message;
}

function removePictures($liste_pictures){
    foreach ($liste_pictures as $picture){
        if($picture['picture']!="defaut.png"){
            //On supprime l'image
            unlink("../img/".$picture['picture']);
        }
    }
}
?>