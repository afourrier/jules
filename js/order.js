/**
 * Created by Alexis on 20/12/2016.
 */
function showOrder(){
    document.getElementById("order").style.visibility = "visible";
    document.getElementById("order").style.display = "block";
    window.location.hash = '#order';
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);

}

function validatePhone(phone) {
    console.log(phone.length);
    if(phone.length>14){
        return false;
    }
    var re = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/;
    console.log(re.test(phone));

    return re.test(phone);
}

function validate() {
    $("#result").text("");
    var email = $("#input-email").val();
    var phone = $("#input-phone").val();
    if (!validateEmail(email)){
        $("#result").text(email + " is not valid");
        $("#result").css("color", "red");
        return false;
    }
    if(!validatePhone(phone)){
        $("#result").text(phone + " is not valid");
        $("#result").css("color", "red");
        return false;

    }
    //TODO verifier heure
    order();
    return false;
}

$("#input-submit").bind("click", validate);

function order() {
    var name = document.getElementById("input-name").value;
    var email = document.getElementById("input-email").value;
    var phone = document.getElementById("input-phone").value;
    var h = document.getElementById("input-hour");
    var hour = h.options[h.selectedIndex].value;
    var message = document.getElementById("input-message").value;
    var d = document.getElementById("datepicker").value;

    console.log("DATE : "+d);

    $.ajax({
        type: "POST",
        url: "new_order.php",
        data: {'name': name, 'email': email, 'phone': phone, 'hour': hour, 'message': message, 'date': d},
        success: function(){
            // Do what you want to do when the session has been updated
            //TODO go to home
            console.log("OK");
            return false;
        }
    });

    return true;
}