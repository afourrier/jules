/**
 * Created by Alexis on 22/12/2016.
 */
function sell(id, type) {
    console.log(id+ " : " + type);
    $.ajax({
        type: "POST",
        url: "manage_orders.php",
        data: {'id': id, 'type': type},
        success: function(){
            // Do what you want to do when the session has been updated
            document.getElementById("order"+id).style.visibility = "invisible";
            document.getElementById("order"+id).style.display = "none";
            console.log("OK");
            return true;
        }
    });

    return false;
}

/*function addCategorie() {
    var name = document.getElementById('newCate').value;
    document.getElementById('newCate').value = '';

    console.log(name);
    return $.ajax({
        type: "POST",
        url: "manage_products.php",
        data: {'type': "cate", 'nom': name}

    });

}

addCategorie().done(function(data) {
    // work with your data came from server
    console.log("DATA : "+data);
    $("<a class='list-group-item'>"+name+"</a>").insertBefore("#divNewCate");
    return true;
});*/

function confirmation(id, type){
    var r = confirm("Are you sure?");
    if (r == true) {
        $.ajax({
            type: "POST",
            url: "manage_products.php",
            data: {'id': id, 'action': "remove",'type':type },
            success: function(){
                console.log("A");
                if(type=='product'){
                    document.getElementById("procuct"+id).style.visibility = "invisible";
                    document.getElementById("procuct"+id).style.display = "none";
                }
                else if(type=='categorie'){

                    var url = 'orders_list.php';
                    var form = $('<form action="' + url + '" method="post">' +
                        '<input type="text" name="page" value="manage" />' +
                        '</form>');
                    $('body').append(form);
                    form.submit();
                }
                return false;
            }
        });
    }

}

$("#inputCategory").keyup(function(event){
    if(event.keyCode == 13){
        console.log("ENTER")
        var newValue = document.getElementById("inputCategory").value;
        var id = document.getElementById("selectedCategory").value;

        $.ajax({
            type: "POST",
            url: "manage_products.php",
            data: {'id': id, 'action': "alter",'type':'categorie', 'value':newValue },
            success: function(){
                return false;
            }
        });
    }
});