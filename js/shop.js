/**
 * Created by Alexis on 20/12/2016.
 */
function addToCart(id) {
    var e = document.getElementById("quantity"+id);
    var quantity = e.options[e.selectedIndex].value;
    console.log("AJAX -> "+id+" : "+quantity);
    $.ajax({
        type: "POST",
        url: "update_cart.php",
        data: {'id': id, 'quantity': quantity},
        success: function(){
            // Do what you want to do when the session has been updated
            var x = document.getElementById("snackbar")
            x.className = "show";
            setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);
            return true;
        }
    });

    return false;
}