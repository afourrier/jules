<?php
session_start();
if(empty($_SESSION['cart'])){
    $_SESSION['cart'] = [];
}

include ('BDD.php');
if (!empty($_POST["categorie"]))
    $categorie_selected = $_POST["categorie"];
else
    $categorie_selected = 1;//TODO modifier

//Recuperation des categories
$reponse = $BDD->query('SELECT * FROM categorie');
$liste_categorie = array();
while ($res = $reponse->fetch(PDO::FETCH_ASSOC)) {
    $liste_categorie[] = $res;
}

//Recuperation des produits de la categorie
$reponse = $BDD->query('SELECT * FROM products WHERE id_categorie = '.$categorie_selected);
$liste_produits = array();
while ($res = $reponse->fetch(PDO::FETCH_ASSOC)) {
    $liste_produits[] = $res;
}


?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Jule's</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/shop-bootstrap.min.css" rel="stylesheet">
<!--    <link href="css/menu-bar.css" rel="stylesheet">-->

    <!-- Custom CSS -->
    <link href="css/shop-homepage.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <?php include('menu_bar.php')?>


    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">Categories</p>
                <div class="list-group">
                    <?php
                    foreach ($liste_categorie as $value){
                        $selected = "list-group-item";
                        if($value['id']==$categorie_selected){
                            $selected = "categorie_selected";
                        }
                        echo '
                        <form id="categorie'.$value["id"].'" action="shop.php" method="post">
                            <input type="hidden" name="categorie" value="'.$value["id"].'"/>
                        </form>';
                        echo '<a href="#" onclick="document.getElementById(\'categorie'.$value["id"].'\').submit()" class="'.$selected.'">'.$value["label"].'</a>';
                        }
                    ?>
                </div>
            </div>

            <div class="col-md-9">

                <div class="row">

                    <?php
                    foreach ($liste_produits as $produit){
                        //TODO modifier photo
                        echo '<div class="col-sm-4 col-lg-4 col-md-4">
                            <div class="thumbnail">
                                <img src="img/'.$produit["picture"].'" alt="" style="height:165px">
                                <div class="caption">
                                    <h4 class="pull-right">$'.number_format ($produit["price"],2).'</h4>
                                    <h4>'.$produit["label"].'</h4>
                                    <p class="description">'.$produit["description"].'</p>
                                    <!--<p class="pull-right"><button>Add to card</button></p>-->
                                    <p class="pull-right">
                                        <a class="btn btn-primary button button_add" onclick="addToCart('.$produit["id"].',1)">
                                            Add to cart
                                        </a>
                                    </p>
                                    <div>
                                        <select class="btn btn-primary selectQ" id="quantity'.$produit["id"].'">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>';
                    }
                    ?>

                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->

    <div class="container">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
        </footer>

    </div>

    <div id="snackbar">Item(s) add!</div>

    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/shop.js"></script>

</body>

</html>
